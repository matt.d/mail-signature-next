import nextJest from "next/jest.js"

const createJestConfig = nextJest({
  dir: "./", // Provide the path to your Next.js app to load next.config.js and .env files in your test environment
})

const config = {
  testEnvironment: "jest-environment-jsdom",
  setupFilesAfterEnv: ["@testing-library/jest-dom/"],
}

export default createJestConfig(config)
