"use client"

import Faq from "./common/components/faq/faq"
import FaqContent from "./common/components/faq/faqContent"
import ResponsiveDeviceScreen from "./common/components/responsiveDeviceScreen"
import { AppStateProvider } from "./common/context/appStateContext"
import useScreenSize from "./common/hooks/useScreenSize"
import LeftPanel from "./modules/leftPanel/leftPanel"
import RightPanel from "./modules/rightPanel/rightPanel"

export default function Home() {
  // Determine if the device used is responsive
  const isResponsive = useScreenSize().width < 1024 ? true : false

  return (
    // Wrapping the entire app with AppStateProvider to provide context to child components
    <AppStateProvider>
      <div data-testid="app" className="box-border">
        <div className="min-w-screen min-h-screen bg-blue-700">
          <main
            className={`transition-padding relative h-screen w-screen overflow-hidden bg-gradient-to-r from-purple-500 to-teal-500 ${
              isResponsive ? "" : "pl-[25%]"
            }`}
          >
            {!isResponsive && <LeftPanel />}
            {!isResponsive && <RightPanel />}
            {isResponsive && <ResponsiveDeviceScreen />}
            <Faq />
            <FaqContent />
          </main>
        </div>
      </div>
    </AppStateProvider>
  )
}
