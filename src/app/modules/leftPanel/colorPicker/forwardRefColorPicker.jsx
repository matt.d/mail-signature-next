import { forwardRef } from "react"
import { HexColorPicker } from "react-colorful"

/**
 * Forwarded Ref Component for the HexColorPicker.
 * @param {Object} props - The component's props.
 * @param {string} props.color - The initial color value in hexadecimal format.
 * @param {Function} props.handlerFunction - The function to handle color changes.
 * @param {boolean} props.displayColorPicker - Determines whether the color picker is displayed.
 * @param {React.Ref} ref - A reference to the component's DOM element.
 * @returns {JSX.Element} The ForwardRefColorPicker component.
 */
const ForwardRefColorPicker = forwardRef(
  function ForwardRefColorPicker(props, ref) {
    return (
      <div ref={ref}>
        <HexColorPicker
          color={"#" + props.color}
          onChange={(color) => {
            props.handlerFunction(color)
          }}
          className={`${
            props.displayColorPicker ? "showColorPicker" : "hideColorPicker"
          }`}
        />
      </div>
    )
  }
)

export default ForwardRefColorPicker
