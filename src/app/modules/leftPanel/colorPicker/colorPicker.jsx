import { useEffect, useRef, useState } from "react"
import { HexColorInput } from "react-colorful"

import { useAppState } from "../../../common/context/appStateContext"
import ForwardRefColorPicker from "./forwardRefColorPicker"

/**
 * Component for displaying and managing a color picker.
 * @param {Object} props - The component's props.
 * @param {string} props.reduceraction - The action to dispatch for updating the state.
 * @param {string} props.id - The unique identifier for the color picker.
 * @param {string} props.title - The title of the color picker.
 * @returns {JSX.Element} The ColorPickerComponent.
 */
export default function ColorPickerComponent(props) {
  const { state, dispatch } = useAppState()
  const [displayColorPicker, setDisplayColorPicker] = useState(false)
  const reduceraction = props.reduceraction
  const backgroundColor =
    state.tableSelected === "style"
      ? state.styleTable[props.id]
      : state.imagesTable[props.id]

  /**
   * Handles the color change and dispatches the action to update the state.
   * @param {string} newHex - The new hexadecimal color value.
   */
  const handleColor = (newHex) => {
    const inputKey = props.id
    const cleanedHex = newHex.replace("#", "")
    dispatch({
      type: reduceraction,
      inputValue: cleanedHex,
      inputKey: inputKey,
    })
  }

  /**
   * Handles changes in the color value.
   * @param {string} color - The new color value in hexadecimal format.
   */
  const handleHexChange = (color) => {
    const inputKey = props.id
    const cleanedHex = color.replace("#", "")
    dispatch({
      type: reduceraction,
      inputValue: cleanedHex,
      inputKey: inputKey,
    })
  }

  /**
   * Toggles the display of the color picker.
   */
  const handleToggleColorPicker = () => {
    setDisplayColorPicker(!displayColorPicker)
  }

  /**
   * Hides the color picker.
   */
  const handleHideColorPicker = () => {
    setDisplayColorPicker(false)
  }

  // Create a ref to access the child ColorPicker component and distinguish between multiple instances.
  // This ensures that actions are applied to the correct ColorPicker.
  const ref = useRef()

  // Add an event listener on each colorpicker while it is created
  useEffect(() => {
    const handleClick = (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        handleHideColorPicker()
      }
    }

    document.addEventListener("click", handleClick, true)

    return () => {
      document.removeEventListener("click", handleClick, true)
    }
  }, [ref])

  return (
    <>
      <div className="h-[auto] w-full px-[3.75rem] py-[1.875rem] will-change-transform">
        <label htmlFor="" className="block pb-4 font-semibold text-white">
          {props.title}
        </label>
        <div className="flex flex-row items-center">
          <div className="pr-2 text-[1.2rem] text-white">#</div>
          <HexColorInput
            color={backgroundColor}
            value={backgroundColor}
            className="editableInput"
            onChange={(color) => {
              handleHexChange(color)
            }}
          />
          <button
            style={{ backgroundColor: `#${backgroundColor}` }}
            onClick={handleToggleColorPicker}
            className={`ml-2 h-10 w-10 cursor-pointer rounded-[50%] border-2 border-solid border-white transition-[background-color] delay-[0s] duration-[0.3s] ease-[ease-out]`}
          >
            <span className="absolute h-px w-px overflow-hidden whitespace-nowrap border-0 p-0">
              Color Picker: Select Theme Color
            </span>
          </button>
        </div>
        <ForwardRefColorPicker
          ref={ref}
          color={backgroundColor}
          handlerFunction={handleColor}
          displayColorPicker={displayColorPicker}
        />
      </div>
    </>
  )
}
