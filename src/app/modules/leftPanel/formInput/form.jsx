import { useState } from "react"

import { useAppState } from "../../../common/context/appStateContext"

/**
 * Component for handling input forms within the email signature generator.
 * @param {Object} props - The component's properties.
 * @param {string} props.label - The label for the input form.
 * @param {string} props.type - The type of the input form.
 * @param {string} props.placeholder - The placeholder for the input form.
 * @param {string} props.id - The unique identifier for the input form.
 * @param {string} props.reduceraction - The action type for the reducer.
 * @returns {JSX.Element} The Form component.
 */
export default function Form(props) {
  const [inputSelected, setInputSelected] = useState(false)
  const { state, dispatch } = useAppState()

  const reduceraction = props.reduceraction
  const initialState = {
    textsTable: {
      firstName: "John",
      lastName: "Smith",
      jobTitle: "Marketer",
      department: "Marketing",
      companyName: "HubSpot",
      officePhoneNumber: "111 222 3333",
      mobilePhoneNumber: "111 222 3333",
      websiteUrl: "www.hubspot.com",
      emailAddress: "john@smith.com",
      address1: "Line 1",
      address2: "Line 2",
      address3: "Line 3",
      address4: "Line 4",
      facebook: "https://www.facebook.com",
      twitter: "https://www.twitter.com",
      linkedin: "https://www.linkedin.com",
      instagram: "https://wwww.instagram.com",
      customfield: "complementary informations",
    },
    imagesTable: {
      profilePicture:
        "https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/placeholders/placeholder-image@2x.png",
      companyLogo:
        "https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/placeholders/logo-placeholder@2x.png",
      customCtaCopy: "",
      customCtaUrl: "",
      ctaColor: "",
      ctaTextColor: "",
      ctaImage: "",
      customCtaUrlV2: "",
    },
  }

  /**
   * Handles input focus and sets the inputSelected state to true.
   */
  const handleInputFocus = () => {
    setInputSelected(true)
  }

  /**
   * Handles input blur and sets the inputSelected state to false.
   */
  const handleInputBlur = () => {
    setInputSelected(false)
  }

  /**
   * Handles input change and dispatches an action to update the state.
   * @param {Object} e - The event object containing the input value.
   */
  const handleInputChange = (e) => {
    const inputKey = props.id
    const inputValue = e.target.value
    dispatch({
      type: reduceraction,
      inputValue: inputValue,
      inputKey: inputKey,
    })
  }

  // Determine which field is filled to display comma between them
  const isAddress1 = props.id === "address1" ? true : false
  const isAddress2 = props.id === "address2" ? true : false
  const isAddress3 = props.id === "address3" ? true : false
  const isAddress4 = props.id === "address4" ? true : false

  // Determine if the input is equal to the intial value
  // If yes -> display it in light gray
  // Else -> dispay it in white
  const isTextsTab = state.tableSelected === "texts"
  const valueForTab = isTextsTab
    ? state.textsTable[props.id]
    : state.imagesTable[props.id]
  const isInitialValue = isTextsTab
    ? initialState.textsTable[props.id] === valueForTab
    : initialState.imagesTable[props.id] === valueForTab
  const value = isInitialValue ? "" : valueForTab

  return (
    <>
      <div
        className={`h-auto w-full px-[3.75rem] will-change-transform ${
          isAddress4
            ? "pb-[4.75rem]"
            : isAddress3
            ? "pb-[1rem]"
            : isAddress2
            ? "pb-[1rem]"
            : isAddress1
            ? "pb-[1rem] pt-[1.875rem]"
            : "py-[1.875rem]"
        }`}
      >
        <div className="relative text-white">
          <label
            htmlFor=""
            className="m-0 flex flex-col p-0 text-[0.9rem] font-semibold"
          >
            <span>{props.label}</span>
            <div>
              <input
                id="inputText"
                type={props.type}
                value={value} // TODO -> works but remove value if the user enter the same value than the initial one set on the initialState const
                placeholder={props.placeholder}
                onFocus={handleInputFocus}
                onBlur={handleInputBlur}
                onChange={handleInputChange}
              />
            </div>
          </label>
          <div className="absolute bottom-3 right-[5px] h-4 w-4">
            <div className="leading-4">
              <svg
                id="formSvg"
                xmlns="http://www.w3.org/2000/svg"
                x="0px"
                y="0px"
                viewBox="0 0 14.13147 14.13062"
                className={`block h-full w-full fill-white ${
                  inputSelected ? "displayFormSvg" : "hideFormSvg"
                }`}
                {...props}
              >
                <path d="M10.588 5.704L4.432 11.86l-.655-.655c.032-.021.068-.035.096-.063L9.95 5.065a.626.626 0 00-.885-.883l-6.077 6.076c-.028.028-.04.064-.063.095l-.654-.654 6.156-6.156a.626.626 0 00-.885-.884L.944 9.257l-.01.016c-.012.013-.018.029-.028.043a.618.618 0 00-.113.22c-.004.016-.016.026-.02.04l-.76 3.807a.626.626 0 00.735.736l3.807-.762c.014-.002.024-.015.038-.02a.618.618 0 00.22-.11c.014-.011.032-.017.046-.03.004-.005.011-.006.015-.01l6.6-6.6a.626.626 0 00-.885-.884zM13.655 2.107L12.025.476a1.627 1.627 0 00-2.3 0l-.694.693a.627.627 0 000 .885l3.045 3.044a.623.623 0 00.885 0l.693-.694c.308-.307.477-.715.477-1.149s-.169-.841-.476-1.148z" />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
