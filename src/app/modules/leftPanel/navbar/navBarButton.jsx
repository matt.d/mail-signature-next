import { useAppState } from "@/app/common/context/appStateContext"

/**
 * Component for rendering a button in the navigation bar.
 * @param {Object} id - The ID of the button.
 * @returns {JSX.Element} The NavBarButton component.
 */
export default function NavBarButton(id) {
  const { dispatch } = useAppState()

  /**
   * Handles the selection of a tab.
   * @param {string} tabName - The name of the selected tab.
   */
  const handleTabSelection = (tabName) => {
    dispatch({ type: "CHANGE_NAVBAR_TAB", tableSelected: tabName })
  }

  return (
    <div className="z-[1] h-full w-full">
      <li className="m-0 flex h-20 w-full cursor-pointer flex-row items-center justify-center border-0 border-b border-r border-solid border-b-[rgb(46,71,93)] border-r-[rgb(46,71,93)] p-0">
        <button
          id={id.id}
          data-testid={id.id}
          onClick={() => {
            handleTabSelection(id.id)
          }}
          className="m-0 box-border h-full w-full cursor-pointer overflow-visible border-[medium] border-none p-0 text-[100%] normal-case leading-[1.15]"
        >
          <span className="absolute h-px w-px overflow-hidden whitespace-nowrap border-0 p-0"></span>
        </button>
      </li>
    </div>
  )
}
