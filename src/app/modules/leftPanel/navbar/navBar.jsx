import { useAppState } from "@/app/common/context/appStateContext"

import NavBarButton from "./navBarButton"

/**
 * Component for rendering the navigation bar.
 * @returns {JSX.Element} The NavBar component.
 */
export default function NavBar() {
  // Define navigation tables
  const navTables = ["layout", "texts", "style", "images"]
  const { state } = useAppState()

  return (
    <div data-testid="navbar">
      <ul className="relative m-0 flex w-full list-none flex-row bg-[rgb(62,89,116)] p-0">
        {/* Active Tab */}
        <li
          className={`${
            state.tableSelected === "layout"
              ? "translate-x-[0%]"
              : state.tableSelected === "texts"
              ? "translate-x-[100%]"
              : state.tableSelected === "style"
              ? "translate-x-[200%]"
              : state.tableSelected === "images"
              ? "translate-x-[300%]"
              : "translate-x-[0%]"
          } absolute left-0 top-0 h-20 w-3/12 bg-[rgb(46,71,93)] transition-transform delay-[0s] duration-[0.3s] ease-[ease-in-out]`}
        ></li>

        {/* Tabs */}
        {navTables.map((navTable) => {
          return <NavBarButton id={navTable} key={navTable} />
        })}
      </ul>
    </div>
  )
}
