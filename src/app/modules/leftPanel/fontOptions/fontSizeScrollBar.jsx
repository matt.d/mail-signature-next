import { useAppState } from "../../../common/context/appStateContext"

/**
 * Component for selecting and changing the font size for the email signature.
 * @returns {JSX.Element} The FontSizeScrollBar component.
 */
export default function FontSizeScrollBar() {
  const { dispatch } = useAppState()

  /**
   * Handles the font size change and dispatches the action to update the state.
   * @param {Object} e - The event object containing the selected font size.
   */
  const handleChangeFontSize = (e) => {
    const fontSize = e.target.id
    const idTextSize = { small: "16px", medium: "18px", large: "20px" }
    const jobTextSize = { small: "12px", medium: "14px", large: "16px" }
    dispatch({
      type: "CHANGE_FONT_SIZE",
      idTextSize: idTextSize[fontSize],
      jobTextSize: jobTextSize[fontSize],
    })
  }

  return (
    <>
      <div className="h-auto w-full px-[3.75rem] py-[1.875rem] will-change-transform">
        <label htmlFor="" className="block pb-4 font-semibold text-white">
          Font Size
        </label>
        <div className="flex items-center justify-around">
          <button
            className="mx- w-20 rounded-md bg-white px-2"
            id="small"
            onClick={handleChangeFontSize}
          >
            Small
          </button>
          <button
            className="mx- w-20 rounded-md bg-white px-2"
            id="medium"
            onClick={handleChangeFontSize}
          >
            Medium
          </button>
          <button
            className="mx- w-20 rounded-md bg-white px-2"
            id="large"
            onClick={handleChangeFontSize}
          >
            Large
          </button>
        </div>
      </div>
    </>
  )
}
