import { useAppState } from "../../../common/context/appStateContext"

/**
 * Component for selecting and changing the font for the email signature.
 * @param {Object} props - The component's props.
 * @param {Function} props.onChangeFont - The function to handle font changes.
 * @returns {JSX.Element} The FontDropdown component.
 */
export default function FontDropdown(props) {
  const { dispatch } = useAppState()

  /**
   * Handles the font change and dispatches the action to update the state.
   * @param {Object} e - The event object containing the selected font value.
   */
  const handleChangeFont = (e) => {
    const fontFamily = e.target.value
    dispatch({
      type: "CHANGE_STYLE_INPUT",
      inputKey: "fontFamily",
      inputValue: fontFamily,
    })
  }

  return (
    <>
      <div className="h-auto w-full px-[3.75rem] py-[1.875rem] will-change-transform">
        <label htmlFor="" className="block pb-4 font-semibold text-white">
          Font
        </label>
        <div className="relative">
          <select
            name="fontFamily"
            id="fontFamily"
            className="h-8 w-full appearance-none rounded-none border-b-[2px] border-solid border-b-[white] bg-[rgb(46,71,93)] text-base text-white"
            onChange={handleChangeFont}
          >
            <option value="Arial">Arial</option>
            <option value="Courier New">Courier New</option>
            <option value="Georgia">Georgia</option>
            <option value="Lucida Sans Unicode">Lucida Sans Unicode</option>
            <option value="Times New Roman">Times New Roman</option>
            <option value="Trebuchet MS">Trebuchet MS</option>
            <option value="Verdana">Verdana</option>
          </select>
          <div className="pointer-events-none absolute right-0 top-2/4 w-[0.6rem] -translate-x-2/4 -translate-y-2/4 rotate-90 fill-[white] leading-4">
            <svg
              width="26px"
              height="34px"
              viewBox="0 0 26 34"
              xmlns="http://www.w3.org/2000/svg"
              className="block h-full w-full"
              {...props}
            >
              <path
                d="M65.307 48.615L41.98 64.494a.81.81 0 01-1.266-.67V32.067a.81.81 0 011.266-.67l23.327 15.88a.81.81 0 010 1.338z"
                transform="translate(-874 -387) translate(834 356)"
                stroke="none"
                strokeWidth={1}
                fillRule="evenodd"
              />
            </svg>
          </div>
        </div>
      </div>
    </>
  )
}
