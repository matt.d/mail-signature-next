import Image from "next/image"

/**
 * Component for rendering a template model.
 *
 * @param {Object} props - Component props.
 * @param {string} props.name - The name of the template model.
 * @param {string} props.imgUrl - The URL of the template model's image.
 * @param {boolean} props.isSelected - Indicates if the template is selected.
 * @param {Function} props.onSelect - Function to handle template selection.
 * @returns {JSX.Element} The TemplateModel component.
 */
export default function TemplateModel({
  name,
  imgUrl,
  isSelected,
  onSelect,
  props,
}) {
  return (
    <div
      id={name}
      className={`h-auto w-full px-[3.75rem] py-[1.875rem] will-change-transform ${
        // className={`h-auto w-full translate-x-full px-[3.75rem] py-[1.875rem] will-change-transform ${
        isSelected ? "selected" : ""
      }`}
      onClick={(event) => {
        event.preventDefault()
        onSelect(name)
      }}
    >
      <label htmlFor={`template-input-${name}`} className="cursor-pointer">
        <span className="pb-4 font-semibold text-white">{name}</span>
        <input
          id={`template-input-${name}`}
          type="radio"
          className="absolute hidden h-px w-px overflow-hidden whitespace-nowrap border-0 p-0"
        />
        <Image
          src={imgUrl}
          alt={`${name} placeholder`}
          width={0}
          height={0}
          className={`h-full w-full transition-opacity delay-[0s] duration-[0.2s] ease-[ease-in] ${
            isSelected ? "opacity-50" : ""
          }`}
        />
        {isSelected && (
          <div className="absolute left-2/4 top-2/4 flex h-8 w-8 -translate-x-2/4 -translate-y-2/4 scale-100 items-center justify-center rounded-[50%] bg-[rgb(11,143,143)] transition-transform delay-[0s] duration-[0.3s] ease-[cubic-bezier(0.64,0.57,0.67,1.53)]">
            <div className="h-[33.3%] w-auto fill-white">
              <svg
                x="0px"
                y="0px"
                className="block h-full w-full"
                viewBox="0 0 12.4938 9.57589"
                {...props}
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M12.494 1.87a.746.746 0 01-.226.548L5.336 9.35a.746.746 0 01-.548.226.746.746 0 01-.548-.226L.226 5.336A.746.746 0 010 4.788c0-.215.075-.398.226-.548l1.096-1.096a.746.746 0 01.548-.226c.215 0 .398.075.548.226l2.37 2.377L10.076.226A.746.746 0 0110.624 0c.215 0 .397.075.548.226l1.096 1.096c.15.15.226.333.226.548z"
                />
              </svg>
            </div>
          </div>
        )}
      </label>
    </div>
  )
}
