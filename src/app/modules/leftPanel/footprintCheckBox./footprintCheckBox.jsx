/**
 * FootprintCheckbox component represents a checkbox for toggling the "Created With HubSpot" option.
 * @param {Object} props - The component's props.
 * @param {Function} props.onToggle - The function to call when the checkbox is toggled.
 * @param {boolean} props.selectedFootprint - A boolean indicating whether the checkbox is selected.
 * @returns {JSX.Element} The FootprintCheckbox component.
 */
export default function FootprintCheckbox({ onToggle, selectedFootprint }) {
  return (
    <div
      id="footprintContent"
      className="h-auto w-full px-[3.75rem] py-[1.875rem] will-change-transform"
    >
      <input
        type="checkbox"
        id="styled-checkbox-viralToggle"
        name="check"
        className="absolute h-px w-px overflow-hidden whitespace-nowrap border-0 p-0"
      />
      <label
        id="footprintInput"
        htmlFor="styled-checkbox-viralToggle"
        className="flex flex-row items-center justify-around text-[0.9rem] font-medium text-white"
      >
        <span className="font-semibold">Created With HubSpot</span>
        <div
          onClick={onToggle}
          className={`relative h-[1.6rem] w-[3.25rem] min-w-[3.25rem] cursor-pointer rounded-[3px] transition-[background-color] delay-[0s] duration-[0.2s] ease-linear ${
            selectedFootprint ? "bg-[rgb(8,112,112)]" : "bg-[rgb(62,89,116)]"
          }`}
        >
          <div
            className={`absolute left-[-0.075rem] top-[-0.075rem] flex h-7 w-7 items-center justify-center rounded-[3px] bg-white transition-transform delay-[0s] duration-[0.2s] ease-[cubic-bezier(0.64,0.57,0.67,1.53)] ${
              selectedFootprint ? "translate-x-full" : "translate-x-0"
            }`}
          >
            <div className="h-3/6 w-6/12 fill-[rgb(8,112,112)]">
              <svg
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                x="0px"
                y="0px"
                viewBox="0 0 15 11"
                enableBackground="new 0 0 15 11"
                xmlSpace="preserve"
                className={`block h-full w-full transition-transform delay-[0s] duration-[0.2s] ease-linear ${
                  selectedFootprint ? "scale-100  " : "scale-0"
                }`}
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M5.56348,8.91911
    c-1.39995-1.50967-2.761-3.01934-4.16095-4.529C0.66367,3.57406-0.50295,4.79812,0.23591,5.61416
    c1.59438,1.71368,3.14988,3.42736,4.74426,5.14103c0.3111,0.32642,0.85552,0.32642,1.16662,0
    c2.87767-3.10094,5.75533-6.20188,8.633-9.26202c0.69997-0.81604-0.42776-2.04009-1.16662-1.22406
    C10.92994,3.16605,8.24671,6.02217,5.56348,8.91911z"
                />
              </svg>
            </div>
          </div>
        </div>
      </label>
    </div>
  )
}
