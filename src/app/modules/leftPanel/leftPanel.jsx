/**
 * LeftPanel component for rendering different content panels based on the selected tab.
 * @module LeftPanel
 */
import { useAppState } from "@/app/common/context/appStateContext"

import ImagesTable from "./leftPanelContent/imagesTable"
import StyleTable from "./leftPanelContent/styleTable"
import TemplateTable from "./leftPanelContent/templateTable"
import TextsTable from "./leftPanelContent/textsTable"
import NavBar from "./navbar/navBar"

/**
 * LeftPanel functional component.
 * @returns {JSX.Element} The LeftPanel component.
 */
export default function LeftPanel() {
  const { state } = useAppState()

  return (
    <div>
      <div className="absolute left-0 top-0 flex h-screen w-1/4 max-w-[400px] translate-x-0 transform flex-col bg-[rgb(46,71,93)] transition-transform duration-300 ease-in-out">
        <NavBar />
        {state.tableSelected === "layout" && <TemplateTable />}
        {state.tableSelected === "texts" && <TextsTable />}
        {state.tableSelected === "style" && <StyleTable />}
        {state.tableSelected === "images" && <ImagesTable />}
      </div>
    </div>
  )
}
