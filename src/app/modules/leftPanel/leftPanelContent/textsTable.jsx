import { motion } from "framer-motion"

import { useAppState } from "@/app/common/context/appStateContext"

import Form from "../formInput/form"
import Title from "../sectionTitle/title"

/**
 * Component for handling the text input table in the email signature generator.
 * @returns {JSX.Element} The TextsTable component.
 */
export default function TextsTable() {
  // Reducer action for this component
  const reduceraction = "CHANGE_TEXTTABLE_INPUT"

  // Define input details for different sections
  const details = [
    { id: "firstName", label: "First Name", type: "text", placeholder: "John" },
    { id: "lastName", label: "Last Name", type: "text", placeholder: "Smith" },
    {
      id: "jobTitle",
      label: "Job Title",
      type: "text",
      placeholder: "Marketer",
    },
    {
      id: "department",
      label: "Department",
      type: "text",
      placeholder: "Marketing",
    },
    {
      id: "companyName",
      label: "Company Name",
      type: "text",
      placeholder: "HubSpot",
    },
    {
      id: "officePhoneNumber",
      label: "Office Phone Number",
      type: "text",
      placeholder: "111 222 3333",
    },
    {
      id: "mobilePhoneNumber",
      label: "Mobile Phone Number",
      type: "text",
      placeholder: "111 222 3333",
    },
    {
      id: "websiteUrl",
      label: "Website URL",
      type: "text",
      placeholder: "www.hubspot.com",
    },
    {
      id: "emailAddress",
      label: "Email Address",
      type: "text",
      placeholder: "john@smith.com",
    },
    {
      id: "address1",
      label: "Address",
      type: "text",
      placeholder: "Address Line 1",
    },
    { id: "address2", label: "", type: "text", placeholder: "Address Line 2" },
    { id: "address3", label: "", type: "text", placeholder: "Address Line 3" },
    { id: "address4", label: "", type: "text", placeholder: "Address Line 4" },
  ]

  const socialLinks = [
    {
      id: "facebook",
      label: "Facebook",
      type: "text",
      placeholder: "https://www.facebook.com",
    },
    {
      id: "twitter",
      label: "Twitter",
      type: "text",
      placeholder: "https://www.twitter.com",
    },
    {
      id: "linkedin",
      label: "Linkedin",
      type: "text",
      placeholder: "https://www.linkedin.com",
    },
    {
      id: "instagram",
      label: "Instagram",
      type: "text",
      placeholder: "https://wwww.instagram.com",
    },
  ]

  const customField = [
    {
      id: "customfield",
      label: "Cutsom Field",
      type: "text",
      placeholder: "Bonus Content",
    },
  ]

  const { state } = useAppState()
  const activeTab = state.tableSelected === "texts" ? "selected" : "unselected"
  const initialPosition = state.oldTableSelected === "layout" ? -400 : 400

  // Animation states for the list container used by framer motion
  const listVariants = {
    selected: { transition: { staggerChildren: 0.08 } },
    unselected: { opacity: 0 },
  }

  // Animation states for the list element used by framer motion
  const itemVariants = {
    selected: {
      x: 0,
      opacity: 1,
      transition: { type: "spring", bounce: 0.3 },
    },
    unselected: {
      x: initialPosition,
      opacity: 0,
      transition: { duration: 0 },
    },
  }

  return (
    <motion.div
      data-testid="texts-table"
      variants={listVariants}
      animate={activeTab}
      className="relative box-border h-full overflow-x-scroll overscroll-contain border-[medium] border-[rgb(46,71,93)] p-0 pb-20"
    >
      <fieldset className="m-0 border-0 p-0">
        <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
          <Title title="Enter Your Signature Details" />
        </motion.div>
        {details.map((li) => (
          <motion.div
            key={li.id}
            variants={itemVariants}
            initial={{ x: initialPosition }}
          >
            <Form
              id={li.id}
              reduceraction={reduceraction}
              label={li.label}
              type={li.type}
              placeholder={li.placeholder}
            />
          </motion.div>
        ))}
      </fieldset>
      <fieldset className="m-0 border-0 p-0">
        <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
          <Title title="Enter Your Signature Details" />
        </motion.div>
        {socialLinks.map((li) => (
          <motion.div
            key={li.id}
            variants={itemVariants}
            initial={{ x: initialPosition }}
          >
            <Form
              id={li.id}
              reduceraction={reduceraction}
              label={li.label}
              type={li.type}
              placeholder={li.placeholder}
            />
          </motion.div>
        ))}
      </fieldset>
      <fieldset className="m-0 border-0 p-0">
        <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
          <Title title="Enter Your Signature Details" />
        </motion.div>
        {customField.map((li) => (
          <motion.div
            key={li.id}
            variants={itemVariants}
            initial={{ x: initialPosition }}
          >
            <Form
              id={li.id}
              reduceraction={reduceraction}
              label={li.label}
              type={li.type}
              placeholder={li.placeholder}
            />
          </motion.div>
        ))}
      </fieldset>
      <div className="block h-24"></div>
    </motion.div>
  )
}
