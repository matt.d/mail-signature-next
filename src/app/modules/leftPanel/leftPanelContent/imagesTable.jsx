import { motion } from "framer-motion"

import { useAppState } from "@/app/common/context/appStateContext"

import ColorPickerComponent from "../colorPicker/colorPicker"
import Form from "../formInput/form"
import Title from "../sectionTitle/title"

/**
 * Component for handling the images table in the email signature generator.
 * @returns {JSX.Element} The ImagesTable component.
 */
export default function ImagesTable() {
  const { state } = useAppState()
  const activeTab = state.tableSelected === "images" ? "selected" : "unselected"
  const initialPosition = -400
  const reduceraction = "CHANGE_IMAGES_INPUT"
  const colorpickers = [
    { id: "ctaColor", title: "Select CTA Color" },
    { id: "ctaTextColor", title: "Select CTA Text Color" },
  ]

  // Animation states for the list container used by framer motion
  const listVariants = {
    selected: { transition: { staggerChildren: 0.08 } },
    unselected: { opacity: 0 },
  }

  // Animation states for the list element used by framer motion
  const itemVariants = {
    selected: {
      x: 0,
      opacity: 1,
      transition: { type: "spring", bounce: 0.3 },
    },
    unselected: {
      x: initialPosition,
      opacity: 0,
      transition: { duration: 0 },
    },
  }

  return (
    <motion.div
      data-testid="images-table"
      variants={listVariants}
      animate={activeTab}
      className="relative box-border h-full overflow-x-scroll overscroll-contain border-[medium] border-[rgb(46,71,93)] p-0"
    >
      <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
        <Title title="Upload Your Custom Signature Images" />
      </motion.div>
      <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
        <Form
          key="profilePicture"
          id="profilePicture"
          reduceraction={reduceraction}
          label="Profile Picture"
          type="text"
          placeholder="https://example.com/images/myphoto.jpg"
        />
      </motion.div>
      <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
        <Form
          key="companyLogo"
          id="companyLogo"
          reduceraction={reduceraction}
          label="Company Logo"
          type="text"
          placeholder="https://example.com/images/myphoto.jpg"
        />
      </motion.div>
      <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
        <Title title="Create a Custom CTA" />
      </motion.div>
      <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
        <Form
          key="customCtaCopy"
          id="customCtaCopy"
          reduceraction={reduceraction}
          label="Custom CTA Copy"
          type="text"
          placeholder="Find Out More"
        />
      </motion.div>
      <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
        <Form
          key="customCtaUrl"
          id="customCtaUrl"
          reduceraction={reduceraction}
          label="Custom CTA URL"
          type="text"
          placeholder="www.hubspot.com"
        />
      </motion.div>
      {colorpickers.map((colorpicker) => {
        return (
          <motion.div
            key={colorpicker.id}
            variants={itemVariants}
            initial={{ x: initialPosition }}
          >
            <ColorPickerComponent
              id={colorpicker.id}
              reduceraction={reduceraction}
              title={colorpicker.title}
            />
          </motion.div>
        )
      })}
      <div className="block h-24"></div>
    </motion.div>
  )
}
