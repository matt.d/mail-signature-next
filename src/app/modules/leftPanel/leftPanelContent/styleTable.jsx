import { motion } from "framer-motion"

import { useAppState } from "@/app/common/context/appStateContext"

import ColorPickerComponent from "../colorPicker/colorPicker"
import FontDropdown from "../fontOptions/fontDropwdown"
import FontSizeScrollBar from "../fontOptions/fontSizeScrollBar"
import Title from "../sectionTitle/title"

/**
 * Component for handling the style table in the email signature generator.
 * @returns {JSX.Element} The StyleTable component.
 */
export default function StyleTable() {
  const { state } = useAppState()
  const activeTab = state.tableSelected === "style" ? "selected" : "unselected"
  const initialPosition = state.oldTableSelected === "images" ? 400 : -400
  const reduceraction = "CHANGE_STYLE_INPUT"

  // Animation states for the list container used by framer motion
  const listVariants = {
    selected: { transition: { staggerChildren: 0.08 } },
    unselected: { opacity: 0 },
  }

  // Animation states for the list element used by framer motion
  const itemVariants = {
    selected: {
      x: 0,
      opacity: 1,
      transition: { type: "spring", bounce: 0.3 },
    },
    unselected: {
      x: initialPosition,
      opacity: 0,
      transition: { duration: 0 },
    },
  }

  const colorpickers = [
    { id: "themeColor", title: "Select Theme Color" },
    { id: "textColor", title: "Select Text Color" },
    { id: "linkColor", title: "Select Link Color" },
  ]

  return (
    <motion.div
      data-testid="style-table"
      variants={listVariants}
      animate={activeTab}
      className="relative box-border h-full overflow-x-scroll overscroll-contain border-[medium] border-[rgb(46,71,93)] p-0"
    >
      <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
        <Title title="Stylize Your Signature" />
      </motion.div>
      {colorpickers.map((colorpicker) => {
        return (
          <motion.div
            key={colorpicker.id}
            variants={itemVariants}
            initial={{ x: initialPosition }}
          >
            <ColorPickerComponent
              id={colorpicker.id}
              reduceraction={reduceraction}
              title={colorpicker.title}
            />
          </motion.div>
        )
      })}
      <motion.div
        key="fontDropdown"
        variants={itemVariants}
        initial={{ x: initialPosition }}
      >
        <FontDropdown />
      </motion.div>
      <motion.div
        key="fontSizeScrollBar"
        variants={itemVariants}
        initial={{ x: initialPosition }}
      >
        <FontSizeScrollBar />
      </motion.div>
      <div className="block h-24"></div>
    </motion.div>
  )
}
