import { motion } from "framer-motion"

import { useAppState } from "@/app/common/context/appStateContext"

import FootprintCheckbox from "../footprintCheckBox./footprintCheckBox"
import Title from "../sectionTitle/title"
import TemplateModel from "../templateModel/templateModel"

/**
 * Component for handling the template table in the email signature generator.
 * @returns {JSX.Element} The TemplateTable component.
 */
export default function TemplateTable() {
  // Array of template data
  const templateData = [
    {
      name: "model1",
      imgUrl:
        "//cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/templates/template-1.svg",
    },
    {
      name: "model2",
      imgUrl:
        "//cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/templates/template-2.svg",
    },
    {
      name: "model3",
      imgUrl:
        "//cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/templates/template-3.svg",
    },
    {
      name: "model4",
      imgUrl:
        "//cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/templates/template-4.svg",
    },
    {
      name: "model5",
      imgUrl:
        "//cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/templates/template-5.svg",
    },
    {
      name: "model6",
      imgUrl:
        "//cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/templates/template-6.svg",
    },
  ]

  const { state, dispatch } = useAppState()
  const activeTab = state.tableSelected === "layout" ? "selected" : "unselected"
  const initialPosition = 400

  // Animation states for the list container used by framer motion
  const listVariants = {
    selected: { transition: { staggerChildren: 0.08 } },
    unselected: { opacity: 0 },
  }

  // Animation states for the list element used by framer motion
  const itemVariants = {
    selected: {
      x: 0,
      opacity: 1,
      transition: { type: "spring", bounce: 0.3 },
    },
    unselected: {
      x: initialPosition,
      opacity: 0,
      transition: { duration: 0 },
    },
  }

  /**
   * Handles the toggle of the footprint checkbox.
   */
  const handleFootprintToggle = () => {
    dispatch({ type: "TOGGLE_FOOTPRINT_CHECKBOX" })
  }

  /**
   * Handles the selection of a template.
   * @param {string} templateName - The name of the selected template.
   */
  const handleTemplateSelection = (templateName) => {
    dispatch({ type: "UPDATE_TEMPLATE", templateSelected: templateName })
  }

  return (
    <motion.div
      data-testid="template-table"
      variants={listVariants}
      animate={activeTab}
      className="relative box-border h-full overflow-x-scroll overscroll-contain border-[medium] border-[rgb(46,71,93)] p-0"
    >
      <fieldset className="m-0 border-0 p-0">
        <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
          <Title title="Select your template" />
        </motion.div>
        <motion.div variants={itemVariants} initial={{ x: initialPosition }}>
          <FootprintCheckbox
            onToggle={handleFootprintToggle}
            selectedFootprint={state.templateTable.isFootprintSelected}
          />
        </motion.div>
        {templateData.map((template) => (
          <motion.div
            key={template.name}
            variants={itemVariants}
            initial={{ x: initialPosition }}
          >
            <TemplateModel
              name={template.name}
              imgUrl={template.imgUrl}
              isSelected={
                state.templateTable.templateSelected === template.name
              }
              onSelect={handleTemplateSelection}
            />
          </motion.div>
        ))}
      </fieldset>
      <div className="block h-24"></div>
    </motion.div>
  )
}
