/**
 * Component for rendering a title.
 *
 * @param {Object} title - The title text.
 * @returns {JSX.Element} The Title component.
 */
export default function Title(title) {
  return (
    <>
      <legend className="box-border h-auto w-full max-w-full whitespace-normal px-[3.75rem] pb-[0.9rem] pt-[1.875rem] will-change-transform">
        <span
          id="chooseText"
          className="w-full animate-[0.3s_linear_0s_1_normal_none_running] pt-4 text-lg font-bold text-white"
        >
          {title.title}
        </span>
      </legend>
    </>
  )
}
