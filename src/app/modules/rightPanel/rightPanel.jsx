/**
 * RightPanel component is responsible for rendering the right panel of the application.
 * @module RightPanel
 */
"use client"

import { useAppState } from "@/app/common/context/appStateContext"

import Model1 from "./mailSignatureModels/model1"
import Model2 from "./mailSignatureModels/model2"
import Model3 from "./mailSignatureModels/model3"
import Model4 from "./mailSignatureModels/model4"
import Model5 from "./mailSignatureModels/model5"
import Model6 from "./mailSignatureModels/model6"
import GetSignature from "./modelUIElements/getSignature"

/**
 * RightPanel functional component.
 *
 * @returns {JSX.Element} The RightPanel component.
 */
export default function RightPanel() {
  const { state } = useAppState()

  return (
    <section className="h-full w-full overflow-y-scroll">
      <div className="flex w-full flex-col items-center justify-center">
        <div className="h-[50px]"></div>
        <div className="w-4/5">
          <div className="relative w-full rounded-[5px]">
            <div className="flex flex-row rounded-[8px_8px_0px_0px] border-b border-solid border-b-[rgba(255,255,255,0.1)] bg-[rgb(46,71,93)] p-4">
              <div className="mr-2 h-2 w-2 rounded-[50%] bg-[rgb(224,43,61)]"></div>
              <div className="mr-2 h-2 w-2 rounded-[50%] bg-[rgb(255,188,75)]"></div>
              <div className="mr-2 h-2 w-2 rounded-[50%] bg-[rgb(11,143,143)]"></div>
            </div>
          </div>
          <div className="flex flex-col bg-[rgb(46,71,93)] px-8 py-4 text-sm font-medium text-white">
            <span>
              A :<strong>Votre destinataire</strong>
            </span>
            <span>
              Objet :<strong>Découvrez ma nouvelle signature de mail</strong>
            </span>
          </div>
        </div>
        <div className="flex w-full justify-center">
          {state.templateTable.templateSelected === "model1" && <Model1 />}
          {state.templateTable.templateSelected === "model2" && <Model2 />}
          {state.templateTable.templateSelected === "model3" && <Model3 />}
          {state.templateTable.templateSelected === "model4" && <Model4 />}
          {state.templateTable.templateSelected === "model5" && <Model5 />}
          {state.templateTable.templateSelected === "model6" && <Model6 />}
        </div>
        <GetSignature />
      </div>
    </section>
  )
}
