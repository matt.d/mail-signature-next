import { useAppState } from "@/app/common/context/appStateContext"

import SocialIcon from "./socialIcon"

/**
 * Socials component is responsible for rendering social media icons with links.
 *
 * @returns {JSX.Element} The Socials component.
 */
export default function Socials() {
  const { state } = useAppState()
  // Determine is logos need to be displayed
  const isFacebook = state.textsTable.facebook === "" ? false : true
  const isTwitter = state.textsTable.twitter === "" ? false : true
  const isLinkedin = state.textsTable.linkedin === "" ? false : true
  const isInstagram = state.textsTable.instagram === "" ? false : true

  const imgUrl = {
    facebookLogo:
      "https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/facebook-icon-2x.png",
    twitterLogo:
      "https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/twitter-icon-2x.png",
    linkedinLogo:
      "https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/linkedin-icon-2x.png",
    instagramLogo:
      "https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/instagram-icon-2x.png",
  }

  const alt = {
    facebookAlt: "facebook icon",
    twitterAlt: "twitter icon",
    linkedinAlt: "linkedin icon",
    instagramAlt: "instagram icon",
  }

  return (
    <tr>
      <td className="text-center">
        <table className="inline-block text-[medium]">
          <tbody>
            <tr className="text-center">
              {isFacebook && (
                <SocialIcon
                  href={state.textsTable.facebook}
                  imgUrl={imgUrl.facebookLogo}
                  alt={alt.facebookAlt}
                />
              )}
              {isTwitter && (
                <SocialIcon
                  href={state.textsTable.twitter}
                  imgUrl={imgUrl.twitterLogo}
                  alt={alt.twitterAlt}
                />
              )}
              {isLinkedin && (
                <SocialIcon
                  href={state.textsTable.linkedin}
                  imgUrl={imgUrl.linkedinLogo}
                  alt={alt.linkedinAlt}
                />
              )}
              {isInstagram && (
                <SocialIcon
                  href={state.textsTable.instagram}
                  imgUrl={imgUrl.instagramLogo}
                  alt={alt.instagramAlt}
                />
              )}
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  )
}
