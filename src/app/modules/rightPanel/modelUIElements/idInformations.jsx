import { useAppState } from "@/app/common/context/appStateContext"

/**
 * IdInformations component is responsible for rendering the identification information.
 * @returns {JSX.Element} The IdInformations component.
 */
export default function IdInformations() {
  const { state } = useAppState()
  const textColor = "#" + state.styleTable.textColor
  const displayBar =
    state.textsTable.department !== "" && state.textsTable.companyName !== ""
      ? true
      : false

  /**
   * Bar component renders a separator bar (|).
   * @returns {JSX.Element} The Bar component.
   */
  const Bar = () => {
    return (
      <>
        <span> | </span>
      </>
    )
  }

  return (
    <>
      <h2
        className="m-0 text-lg font-semibold"
        style={{
          color: `${textColor}`,
          fontFamily: `${state.styleTable.fontFamily}`,
          fontSize: `${state.styleTable.idTextSize}`,
        }}
      >
        <span>{state.textsTable.firstName}</span>
        <span> </span>
        <span>{state.textsTable.lastName}</span>
      </h2>
      <p
        className="m-0 text-sm leading-[22px]"
        style={{
          color: `${textColor}`,
          fontFamily: `${state.styleTable.fontFamily}`,
          fontSize: `${state.styleTable.jobTextSize}`,
        }}
      >
        <span>{state.textsTable.jobTitle}</span>
      </p>
      <p
        className="m-0 text-sm font-medium leading-[22px]"
        style={{
          color: `${textColor}`,
          fontFamily: `${state.styleTable.fontFamily}`,
          fontSize: `${state.styleTable.jobTextSize}`,
        }}
      >
        <span>{state.textsTable.department}</span>
        {displayBar && <Bar />}
        <span>{state.textsTable.companyName}</span>
      </p>
      <p
        className="m-0 text-sm leading-[22px]"
        style={{
          color: `${textColor}`,
          fontFamily: `${state.styleTable.fontFamily}`,
          fontSize: `${state.styleTable.jobTextSize}`,
        }}
      >
        <span>{state.textsTable.customfield}</span>
      </p>
    </>
  )
}
