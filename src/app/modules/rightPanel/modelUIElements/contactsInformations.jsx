import Image from "next/image"

import { useAppState } from "@/app/common/context/appStateContext"

/**
 * ContactInformations component represents the contact information section in the email signature.
 * @returns {JSX.Element} The ContactInformations component.
 */
export default function ContactInformations() {
  const { state } = useAppState()
  const themeColor = "#" + state.styleTable.themeColor
  const textColor = "#" + state.styleTable.textColor

  // Determine whether to display the phone number bar.
  const displayBar =
    state.textsTable.officePhoneNumber !== "" &&
    state.textsTable.mobilePhoneNumber !== ""
      ? true
      : false

  // Separators for multiple address components.
  const Bar = () => {
    return (
      <>
        <span> | </span>
      </>
    )
  }

  // Check if each address component has content.
  const isAddress1 = state.textsTable.address1 !== ""
  const isAddress2 = state.textsTable.address2 !== ""
  const isAddress3 = state.textsTable.address3 !== ""
  const isAddress4 = state.textsTable.address4 !== ""

  // Determine whether to display commas between address components.
  const displayComma1 = isAddress1 && isAddress2
  const displayComma2 = isAddress3 && (isAddress1 || isAddress2)
  const displayComma3 = isAddress4 && (isAddress1 || isAddress2 || isAddress3)

  // Component for separating address components with commas.
  const Comma = () => {
    return (
      <>
        <span>, </span>
      </>
    )
  }

  // Check if each contact information category has content.
  const isPhone =
    state.textsTable.officePhoneNumber !== "" ||
    state.textsTable.mobilePhoneNumber !== ""
  const isEmail = state.textsTable.emailAddress !== ""
  const isWebsite = state.textsTable.websiteUrl !== ""
  const isAddress = isAddress1 || isAddress2 || isAddress3 || isAddress4

  // Component for the phone icon.
  const PhoneImage = () => {
    return (
      <>
        <td width={30} className="align-middle">
          <table className="font-medium">
            <tbody>
              <tr>
                <td className="align-bottom">
                  <span className="inline-block bg-[rgb(53,52,113)]">
                    <Image
                      src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/phone-icon-2x.png"
                      alt="Mobile phone logo"
                      width={13}
                      height={13}
                      style={{ background: `${themeColor}` }}
                      className="block"
                    />
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </>
    )
  }

  // Component for the email icon.
  const EmailImage = () => {
    return (
      <>
        <td width={30} className="align-middle">
          <table className="font-medium">
            <tbody>
              <tr>
                <td className="align-bottom">
                  <span className="inline-block bg-[rgb(53,52,113)]">
                    <Image
                      src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/email-icon-2x.png"
                      alt="Email logo"
                      width={13}
                      height={13}
                      style={{ background: `${themeColor}` }}
                      className="block"
                    />
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </>
    )
  }

  // Component for the website icon.
  const WebsiteImage = () => {
    return (
      <>
        <td width={30} className="align-middle">
          <table className="font-medium">
            <tbody>
              <tr>
                <td className="align-bottom">
                  <span className="inline-block bg-[rgb(53,52,113)]">
                    <Image
                      src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/link-icon-2x.png"
                      alt="Hyperlink logo"
                      width={13}
                      height={13}
                      style={{ background: `${themeColor}` }}
                      className="block"
                    />
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </>
    )
  }

  // Component for the address icon.
  const AddressImage = () => {
    return (
      <>
        <td width={30} className="align-middle">
          <table className="font-medium">
            <tbody>
              <tr>
                <td className="align-bottom">
                  <span className="inline-block bg-[rgb(53,52,113)]">
                    <Image
                      src="https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/icons/address-icon-2x.png"
                      alt="Address logo"
                      width={13}
                      height={13}
                      style={{ background: `${themeColor}` }}
                      className="block"
                    />
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </>
    )
  }

  return (
    <table
      style={{ fontFamily: `${state.styleTable.fontFamily}` }}
      className="font-medium"
    >
      <tbody>
        <tr height={25} className="align-middle">
          {isPhone && <PhoneImage />}
          <td style={{ color: `${textColor}` }} className="p-0">
            <a
              href="tel :" // TODO -> update the href
              color="#6f66ba"
              style={{ color: `${textColor}` }}
              className="text-xs no-underline"
            >
              <span>{state.textsTable.officePhoneNumber}</span>
            </a>
            {displayBar && <Bar />}
            <a
              href="" // TODO -> update the href
              style={{ color: `${textColor}` }}
              className="text-xs no-underline"
            >
              <span> {state.textsTable.mobilePhoneNumber}</span>
            </a>
          </td>
        </tr>
        <tr height={25} className="align-middle">
          {isEmail && <EmailImage />}
          <td style={{ color: `${textColor}` }} className="p-0">
            <a
              href="tel :"
              color="#6f66ba"
              style={{ color: `${textColor}` }}
              className="text-xs no-underline"
            >
              <span>{state.textsTable.emailAddress}</span>
            </a>
          </td>
        </tr>
        <tr height={25} className="align-middle">
          {isWebsite && <WebsiteImage />}
          <td style={{ color: `${textColor}` }} className="p-0">
            <a
              href="tel :"
              color="#6f66ba"
              style={{ color: `${textColor}` }}
              className="text-xs no-underline"
            >
              <span>{state.textsTable.websiteUrl}</span>
            </a>
          </td>
        </tr>
        <tr height={25} className="align-middle">
          {isAddress && <AddressImage />}
          <td style={{ color: `${textColor}` }} className="p-0">
            <a
              href="tel :"
              color="#6f66ba"
              style={{ color: `${textColor}` }}
              className="text-xs no-underline"
            >
              <span>
                {state.textsTable.address1}
                {displayComma1 && <Comma />}
                {state.textsTable.address2}
                {displayComma2 && <Comma />}
                {state.textsTable.address3}
                {displayComma3 && <Comma />}
                {state.textsTable.address4}
              </span>
            </a>
          </td>
        </tr>
      </tbody>
    </table>
  )
}
