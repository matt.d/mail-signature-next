import Image from "next/image"

import { useAppState } from "@/app/common/context/appStateContext"

/**
 * SocialIcon component is responsible for rendering a social media icon with a link.
 *
 * @param {object} props - The component's properties.
 * @param {string} props.href - The URL link to the social media profile.
 * @param {string} props.imgUrl - The URL of the social media icon image.
 * @param {string} props.alt - The alternative text for the social media icon.
 * @returns {JSX.Element} The SocialIcon component.
 */
export default function SocialIcon(props) {
  const { state } = useAppState()
  const linkColor = "#" + state.styleTable.linkColor

  return (
    <>
      <td>
        <a
          href={props.href}
          style={{ backgroundColor: `${linkColor}` }}
          className="inline-block p-0"
        >
          <Image
            src={props.imgUrl}
            alt={props.alt}
            color="#6a78d1"
            width={24}
            height={24}
            className="block h-[24px] max-w-[135px]"
          />
        </a>
      </td>
      <td width={5}></td>
    </>
  )
}
