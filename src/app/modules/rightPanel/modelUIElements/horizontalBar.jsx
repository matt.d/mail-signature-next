import { useAppState } from "@/app/common/context/appStateContext"

/**
 * HorizontalBar component is responsible for rendering a horizontal bar with a theme color border.
 * @returns {JSX.Element} The HorizontalBar component.
 */
export default function HorizontalBar() {
  const { state } = useAppState()
  const themeColor = "#" + state.styleTable.themeColor

  return (
    <tr>
      <td>
        <table className="w-full">
          <tbody>
            <tr>
              <td height={30}></td>
            </tr>
            <tr>
              <td
                color={themeColor}
                direction="horizontal"
                height={1}
                style={{ borderBottom: `1px solid ${themeColor}` }}
                className={`block w-full`}
              ></td>
            </tr>
            <tr>
              <td height={30}></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  )
}
