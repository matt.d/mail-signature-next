import { useAppState } from "@/app/common/context/appStateContext"

/**
 * FootPrint component represents the "Create Your Own Free Signature" section in the email signature.
 * @returns {JSX.Element} The FootPrint component.
 */
export default function FootPrint() {
  const { state } = useAppState()
  const textColor = "#" + state.styleTable.textColor

  return (
    <div>
      {state.templateTable.isFootprintSelected && (
        <table className="w-full text-[medium]">
          <tbody>
            <tr>
              <td height={15}></td>
            </tr>
            <tr>
              <td>
                <a
                  href="https://www.hubspot.com/email-signature-generator"
                  style={{ color: `${textColor}` }}
                  className="block text-xs"
                >
                  Create Your Own Free Signature
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      )}
    </div>
  )
}
