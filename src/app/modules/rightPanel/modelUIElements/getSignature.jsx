import { useEffect, useRef, useState } from "react"

import { useAppState } from "@/app/common/context/appStateContext"

import Model1 from "../mailSignatureModels/model1"
import Model2 from "../mailSignatureModels/model2"
import Model3 from "../mailSignatureModels/model3"
import Model4 from "../mailSignatureModels/model4"
import Model5 from "../mailSignatureModels/model5"
import Model6 from "../mailSignatureModels/model6"

/**
 * GetSignature component is responsible for generating and managing email signature models.
 * @returns {JSX.Element} The GetSignature component.
 */
export default function GetSignature() {
  const { state, dispatch } = useAppState()
  const [selectedModel, setSelectedModel] = useState(null)
  // References to each models which is useful to retrieve jsx from
  const refModels = {
    model1: useRef(null),
    model2: useRef(null),
    model3: useRef(null),
    model4: useRef(null),
    model5: useRef(null),
    model6: useRef(null),
  }

  /**
   * Resets all the values in the email signature form.
   */
  const handleResetValues = () => {
    dispatch({ type: "RESET_VALUES" })
  }

  /**
   * Copies the generated email signature to the clipboard and provides instructions to the user.
   */
  const copyToClipboard = () => {
    const componentElement = refModels[selectedModel].current

    // Create DOM element
    const dummyTextArea = document.createElement("textarea")
    document.body.appendChild(dummyTextArea)
    dummyTextArea.value = componentElement.innerHTML

    // Perform copy to clipboard
    dummyTextArea.select()
    document.execCommand("copy")
    document.body.removeChild(dummyTextArea)
    alert(
      "Congrats! Your signature is copied to the clipboard:\n\
            1. Save it into an HTML file\n\
            2. Open this file in your browser\n\
            3. Go to your mailbox settings and navigate to the signature tab.\n\
            4. Paste the signature from the browser into it\n\
            5. Enjoy :)"
    )
  }

  // Determine which model jsx to copy based on the model selected from the template table
  useEffect(() => {
    setSelectedModel(state.templateTable.templateSelected)
  }, [state.templateTable.templateSelected])

  return (
    <div>
      <div className="mx-0 my-12 flex flex-col items-center justify-center">
        <button
          onClick={copyToClipboard}
          className="h-[50px] w-[220px] cursor-pointer rounded-[3px] border-none bg-[rgb(51,71,91)] text-base font-semibold text-white"
        >
          Create Signature
        </button>
        <button
          onClick={handleResetValues}
          className="mt-2 cursor-pointer border-none px-8 py-4 text-xs font-semibold uppercase text-white"
        >
          <div className="mr-3 inline-block h-3 w-3 fill-white">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              x="0px"
              y="0px"
              viewBox="0 0 10 12"
              xmlSpace="preserve"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M8.334 6.91c0 1.871-1.496 3.393-3.334 3.393S1.666 8.781 1.666 6.91 3.162 3.516 5 3.516V4.89c0 .115.043.228.128.315.17.174.447.174.618 0L7.93 2.982a.45.45 0 000-.629L5.746.13a.431.431 0 00-.618 0 .443.443 0 00-.126.326L5 .974v.833c-2.757.012-5 2.296-5 5.103S2.243 12 5 12s5-2.284 5-5.09a.84.84 0 00-.833-.849.84.84 0 00-.833.849z"
              />
            </svg>
          </div>
          RESET ALL FIELDS
        </button>
      </div>
      {/* Hidden models from which to copy jsx by using reference*/}
      <div id="anchor" className="hidden">
        {selectedModel === "model1" && <Model1 ref={refModels.model1} />}
        {selectedModel === "model2" && <Model2 ref={refModels.model2} />}
        {selectedModel === "model3" && <Model3 ref={refModels.model3} />}
        {selectedModel === "model4" && <Model4 ref={refModels.model4} />}
        {selectedModel === "model5" && <Model5 ref={refModels.model5} />}
        {selectedModel === "model6" && <Model6 ref={refModels.model6} />}
      </div>
    </div>
  )
}
