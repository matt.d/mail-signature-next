import { forwardRef } from "react"
import Image from "next/image"

import { useAppState } from "@/app/common/context/appStateContext"

import ContactInformations from "../modelUIElements/contactsInformations"
import FootPrint from "../modelUIElements/footPrint"
import HorizontalBar from "../modelUIElements/horizontalBar"
import IdInformations from "../modelUIElements/idInformations"
import Socials from "../modelUIElements/socials"
import Cta from "./cta"

/**
 * Model1 component represents a specific email signature template.
 * @param {object} props - Component props.
 * @param {object} ref - A ref to the component.
 * @returns {JSX.Element} The Model1 component.
 */
const Model1 = forwardRef(function Model1(props, ref) {
  const { state } = useAppState()

  return (
    <div className="w-4/5">
      <div className="rounded-[0px_0px_8px_8px] bg-white">
        <div
          id="signature-wrapper"
          className="relative flex w-full items-center justify-center overflow-hidden px-10 py-[5.625rem]"
        >
          <div className="select-none text-base">
            <table ref={ref} className="text-[medium]">
              <tbody className="border-0 align-baseline">
                <tr>
                  <td>
                    <table className="text-[medium]">
                      <tbody>
                        <tr>
                          <td className="align-top">
                            <table className="font-medium">
                              <tbody>
                                <tr>
                                  <td>
                                    <Image
                                      src={state.imagesTable.profilePicture}
                                      alt="Profile picture"
                                      width={130}
                                      height={130}
                                      className="block max-w-[130px]"
                                    />
                                  </td>
                                </tr>
                                <tr>
                                  <td height={30}></td>
                                </tr>
                                <tr>
                                  <td>
                                    <Image
                                      src={state.imagesTable.companyLogo}
                                      alt="Company logo"
                                      width={130}
                                      height={130}
                                    />
                                  </td>
                                </tr>
                                <tr>
                                  <td height={30}></td>
                                </tr>
                                <Socials />
                              </tbody>
                            </table>
                          </td>
                          <td width={46}></td>
                          <td className="align-top">
                            <IdInformations />
                            <table className="w-full text-[medium]">
                              <tbody>
                                <HorizontalBar />
                              </tbody>
                            </table>
                            <ContactInformations />
                            <Cta textAlign="text-left" />
                            <FootPrint />
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
})

export default Model1
