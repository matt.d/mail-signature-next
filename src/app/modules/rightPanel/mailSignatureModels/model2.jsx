import { forwardRef } from "react"
import Image from "next/image"

import { useAppState } from "@/app/common/context/appStateContext"

import ContactInformations from "../modelUIElements/contactsInformations"
import FootPrint from "../modelUIElements/footPrint"
import HorizontalBar from "../modelUIElements/horizontalBar"
import IdInformations from "../modelUIElements/idInformations"
import Socials from "../modelUIElements/socials"
import Cta from "./cta"

/**
 * Model2 component represents another specific email signature template.
 * @param {object} props - Component props.
 * @param {object} ref - A ref to the component.
 * @returns {JSX.Element} The Model2 component.
 */
const Model2 = forwardRef(function Model2(props, ref) {
  const { state } = useAppState()

  return (
    <div ref={ref} className="w-4/5">
      <div className="rounded-[0px_0px_8px_8px] bg-white">
        <div
          id="signature-wrapper"
          className="relative flex w-full items-center justify-center overflow-hidden px-10 py-[5.625rem]"
        >
          <div className="select-none text-base">
            <table className="min-w-[450px] text-[medium]">
              <tbody className="border-0 align-baseline">
                <tr>
                  <td>
                    <Image
                      src={state.imagesTable.profilePicture}
                      alt="Profile picture"
                      width={130}
                      height={130}
                      className="block max-w-[130px]"
                    />
                  </td>
                </tr>
                <IdInformations className="text-center" />
                <HorizontalBar />
                <tr>
                  <table>
                    <tbody>
                      <tr>
                        <ContactInformations />
                        <td width={15}></td>
                        <td>
                          <table>
                            <tbody>
                              <tr>
                                <td>
                                  <Image
                                    src={state.imagesTable.companyLogo}
                                    alt="Company logo"
                                    width={130}
                                    height={130}
                                    className="block max-w-[130px]"
                                  />
                                </td>
                              </tr>
                              <tr>
                                <td height={10}></td>
                              </tr>
                              <Socials />
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </tr>
                <HorizontalBar />
                <Cta textAlign="text-center" />
                <FootPrint />
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
})

export default Model2
