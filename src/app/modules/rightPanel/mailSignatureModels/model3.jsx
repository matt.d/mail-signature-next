import { forwardRef } from "react"
import Image from "next/image"

import { useAppState } from "@/app/common/context/appStateContext"

import ContactInformations from "../modelUIElements/contactsInformations"
import FootPrint from "../modelUIElements/footPrint"
import HorizontalBar from "../modelUIElements/horizontalBar"
import IdInformations from "../modelUIElements/idInformations"
import Socials from "../modelUIElements/socials"
import Cta from "./cta"

/**
 * Model3 component represents another specific email signature template.
 * @param {object} props - Component props.
 * @param {object} ref - A ref to the component.
 * @returns {JSX.Element} The Model2 component.
 */
const Model3 = forwardRef(function Model3(props, ref) {
  const { state } = useAppState()

  return (
    <div ref={ref} className="w-4/5">
      <div className="rounded-[0px_0px_8px_8px] bg-white">
        <div
          id="signature-wrapper"
          className="relative flex w-full items-center justify-center overflow-hidden px-10 py-[5.625rem]"
        >
          <div className="select-none text-base">
            <table className="text-[medium]">
              <tbody className="border-0 align-baseline">
                <tr>
                  <td>
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <Image
                              src={state.imagesTable.profilePicture}
                              alt="Profile picture"
                              width={130}
                              height={130}
                              className="block max-w-[130px]"
                            />
                          </td>
                          <IdInformations />
                          <td width={30}></td>
                          <td
                            color="#f2547d"
                            direction="vertical"
                            width={1}
                            className=" block border-b border-l border-solid border-b-[rgb(53,52,113)] border-l-[rgb(53,52,113)]"
                          ></td>
                          <td width={30}></td>
                          <ContactInformations />
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <HorizontalBar />
                <tr>
                  <td>
                    <table className="w-full">
                      <tbody>
                        <tr>
                          <td>
                            <Image
                              src={state.imagesTable.companyLogo}
                              alt="Company logo"
                              width={130}
                              height={130}
                              className="block max-w-[130px]"
                            />
                          </td>
                          <Socials />
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <FootPrint />
                <Cta textAlign="text-left" />
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
})

export default Model3
