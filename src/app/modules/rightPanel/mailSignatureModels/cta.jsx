import { useAppState } from "@/app/common/context/appStateContext"

/**
 * Cta component represents the Call to Action (CTA) section in the email signature.
 * @param {Object} props - The component props.
 * @param {string} props.textAlign - The text alignment for the CTA button.
 * @returns {JSX.Element|null} The Cta component or null if there is no custom CTA.
 */
export default function Cta(props) {
  const { state } = useAppState()
  const ctaTextColor = "#" + state.imagesTable.ctaTextColor // Format the hexadecimal CTA text color value
  const ctaColor = "#" + state.imagesTable.ctaColor // Format the hexadecimal CTA background color value
  const isCta = state.imagesTable.customCtaCopy === "" ? false : true // Bool to display CTA or not

  /**
   * CtaButton component renders the custom CTA button.
   * @param {Object} props - The component props.
   * @param {string} props.textAlign - The text alignment for the CTA button.
   * @returns {JSX.Element} The CtaButton component.
   */
  const CtaButton = (props) => {
    console.log(props.textAlign)

    return (
      <>
        <span className={`block ${props.textAlign}`}>
          <a
            href={state.imagesTable.customCtaUrl}
            className="inline-block rounded-[3px] border-solid border-[6px_12px] px-2 py-2 text-center text-xs font-bold leading-10 no-underline"
            style={{
              color: `${ctaTextColor}`,
              backgroundColor: `${ctaColor}`,
              borderColor: `${ctaColor}`,
            }}
            target="_blank"
          >
            {state.imagesTable.customCtaCopy}
          </a>
        </span>
      </>
    )
  }

  return <>{isCta && <CtaButton textAlign={props.textAlign} />}</>
}
