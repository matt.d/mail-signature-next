import React from "react"
import { render, screen } from "@testing-library/react"

import Home from "@/app/page"

test("Home component renders a heading with the correct text", () => {
  render(<Home />)

  const textsTable = screen.getByTestId("app")
  expect(textsTable).toBeInTheDocument()
})
