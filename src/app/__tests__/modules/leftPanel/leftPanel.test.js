import React from "react"
import { fireEvent, render, screen } from "@testing-library/react"

import {
  AppStateProvider,
  useAppState,
} from "@/app/common/context/appStateContext"
import LeftPanel from "@/app/modules/leftPanel/leftPanel"

describe("LeftPanel Component", () => {
  it("should display TextsTable when the 'texts' tab is selected", () => {
    // Set up a testing context with the initial state
    const TestComponent = () => {
      return (
        <AppStateProvider>
          <LeftPanel />
        </AppStateProvider>
      )
    }

    const { rerender } = render(<TestComponent />)

    // Simulate a user interaction with the 'texts' tab in the navbar
    fireEvent.click(screen.getByTestId("layout"))

    // Rerender the component to reflect the state change
    rerender(<TestComponent />)

    // Use a test ID or other appropriate selectors to locate the TextsTable component
    const textsTable = screen.getByTestId("template-table")

    expect(textsTable).toBeInTheDocument()
  })

  it("should display TextsTable when the 'texts' tab is selected", () => {
    // Set up a testing context with the initial state
    const TestComponent = () => {
      return (
        <AppStateProvider>
          <LeftPanel />
        </AppStateProvider>
      )
    }

    const { rerender } = render(<TestComponent />)

    // Simulate a user interaction with the 'texts' tab in the navbar
    fireEvent.click(screen.getByTestId("texts"))

    // Rerender the component to reflect the state change
    rerender(<TestComponent />)

    // Use a test ID or other appropriate selectors to locate the TextsTable component
    const textsTable = screen.getByTestId("texts-table")

    expect(textsTable).toBeInTheDocument()
  })

  it("should display TextsTable when the 'texts' tab is selected", () => {
    // Set up a testing context with the initial state
    const TestComponent = () => {
      return (
        <AppStateProvider>
          <LeftPanel />
        </AppStateProvider>
      )
    }

    const { rerender } = render(<TestComponent />)

    // Simulate a user interaction with the 'texts' tab in the navbar
    fireEvent.click(screen.getByTestId("style"))

    // Rerender the component to reflect the state change
    rerender(<TestComponent />)

    // Use a test ID or other appropriate selectors to locate the TextsTable component
    const textsTable = screen.getByTestId("style-table")

    expect(textsTable).toBeInTheDocument()
  })

  it("should display TextsTable when the 'texts' tab is selected", () => {
    // Set up a testing context with the initial state
    const TestComponent = () => {
      return (
        <AppStateProvider>
          <LeftPanel />
        </AppStateProvider>
      )
    }

    const { rerender } = render(<TestComponent />)

    // Simulate a user interaction with the 'texts' tab in the navbar
    fireEvent.click(screen.getByTestId("images"))

    // Rerender the component to reflect the state change
    rerender(<TestComponent />)

    // Use a test ID or other appropriate selectors to locate the TextsTable component
    const textsTable = screen.getByTestId("images-table")

    expect(textsTable).toBeInTheDocument()
  })
})
