import { motion } from "framer-motion"

import { useAppState } from "../../context/appStateContext"

/**
 * FAQ component for displaying frequently asked questions.
 * @returns {JSX.Element} The FAQ component.
 */
export default function Faq() {
  const { dispatch } = useAppState()

  /**
   * Handles the click event to toggle the FAQ content.
   */
  const handleFaqDisplayed = () => {
    dispatch({ type: "TOGGLE_FAQ_CONTENT" })
  }

  return (
    <motion.button
      href=""
      whileTap={{ scale: 1.2 }}
      className="absolute bottom-8 right-8 flex h-[4.5rem] w-[4.5rem] cursor-pointer items-center justify-center rounded-[50%] bg-white text-sm font-semibold text-[rgb(46,71,93)] no-underline shadow-[rgba(0,0,0,0.2)_0px_3px_4px_0px]          "
      onClick={handleFaqDisplayed}
    >
      FAQs
    </motion.button>
  )
}
