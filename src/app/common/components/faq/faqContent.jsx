"use client"

import { motion } from "framer-motion"

import { useAppState } from "../../context/appStateContext"

/**
 * FAQ Content component for displaying the FAQ content.
 * @param {object} props - The component's props.
 * @returns {JSX.Element} The FAQ Content component.
 */
export default function FaqContent(props) {
  const { state, dispatch } = useAppState()

  /**
   * Handles the click event to toggle the FAQ content.
   */
  const handleFaqDisplayed = () => {
    dispatch({ type: "TOGGLE_FAQ_CONTENT" })
  }

  // Animations state for the main container used by framer motion
  const containerVariant = {
    visible: {
      y: 0,
      display: "block",
      transition: { ease: "easeIn", duration: 1 },
    },
    hidden: {
      y: "-100%",
      transition: { ease: "easeOut", duration: 1 },
    },
  }

  // Animations state for the svg tag used by framer motion
  const svgVariant = {
    visible: {
      height: "200%",
      transition: { ease: "easeIn", duration: 1 },
    },
    hidden: {
      height: "0%",
      transition: { ease: "easeOut", duration: 1 },
    },
  }

  return (
    <>
      <motion.div
        initial="hidden"
        animate={state.displayFaq ? "visible" : "hidden"}
        variants={containerVariant}
        className="fixed left-0 top-0 z-10 block h-screen w-screen bg-white"
      >
        <div className="h-full w-full overflow-auto">
          <button className="absolute right-8 top-8 cursor-pointer border-[none]">
            <svg
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              className="h-8 w-8 fill-[#2e475d]"
              onClick={handleFaqDisplayed}
              {...props}
            >
              <path d="M.44 23.561c.293.293.677.439 1.06.439s.767-.146 1.06-.439l9.44-9.44 9.44 9.44c.293.293.676.439 1.06.439a1.5 1.5 0 001.06-2.56L14.121 12l9.44-9.44A1.5 1.5 0 1021.438.44L12 9.879 2.56.439A1.5 1.5 0 10.44 2.562L9.877 12 .44 21.44a1.5 1.5 0 000 2.121" />
            </svg>
          </button>
          <aside>
            <div className="mx-auto mb-48 mt-0 w-full max-w-[680px] pt-40">
              <span>
                <h2 className="mx-0 my-[5.6rem] text-center text-[2rem] font-semibold leading-[1.3125]">
                  FAQ Content
                </h2>
                <h3 className="text-2xl font-medium leading-[1.4167]">
                  WIP - content is comming ...
                </h3>
              </span>
            </div>
          </aside>
        </div>
        <motion.svg
          initial="hidden"
          animate={state.displayFaq ? "visible" : "hidden"}
          variants={svgVariant}
          className="pointer-events-none absolute left-0 top-[99%] z-[-1] h-full w-full origin-[50%_0_0] fill-white"
          viewBox="0 0 1440 800"
          preserveAspectRatio="none"
          transform="scale(1 1.5)"
          {...props}
        >
          <path d="M-44-50c-93.1 167.4 111.86 495.5 280 502 199.3 7.7 264.5-209.4 440-208 197.5 1.6 281 278.4 478 350 439 159.7 639-367.7 428-720S219.8-524.2-44-50z" />
        </motion.svg>
      </motion.div>
    </>
  )
}
