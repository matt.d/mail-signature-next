/**
 * ResponsiveDeviceScreen component for displaying a message for unsupported mobile screens.
 * @returns {JSX.Element} The ResponsiveDeviceScreen component.
 */
export default function ResponsiveDeviceScreen() {
  return (
    <div className="flex h-full w-full flex-col items-center justify-center pb-[25%]">
      <span className="mx-auto my-0 w-4/5 pb-4 text-center text-xl font-medium leading-[2.125rem] text-white">
        Email Signature Template Generator isn&apos;t available on mobile yet.
        We hope you&apos;ll try it out on a larger screen.
      </span>
    </div>
  )
}
