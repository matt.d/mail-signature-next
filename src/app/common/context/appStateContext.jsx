"use client"

import React, { createContext, useContext, useReducer } from "react"

/**
 * Initial state for the application.
 */
const initialState = {
  displayFaq: false,
  tableSelected: "layout",
  oldTableSelected: "layout",
  templateTable: {
    isFootprintSelected: true,
    templateSelected: "model1",
  },
  textsTable: {
    firstName: "John",
    lastName: "Smith",
    jobTitle: "Marketer",
    department: "Marketing",
    companyName: "HubSpot",
    officePhoneNumber: "111 222 3333",
    mobilePhoneNumber: "111 222 3333",
    websiteUrl: "www.hubspot.com",
    emailAddress: "john@smith.com",
    address1: "Line 1",
    address2: "Line 2",
    address3: "Line 3",
    address4: "Line 4",
    facebook: "https://www.facebook.com",
    twitter: "https://www.twitter.com",
    linkedin: "https://www.linkedin.com",
    instagram: "https://wwww.instagram.com",
    customfield: "complementary informations",
  },
  styleTable: {
    themeColor: "F2547D",
    textColor: "000000",
    linkColor: "6A78D1",
    fontFamily: "Arial",
    idTextSize: "16px",
    jobTextSize: "12px",
  },
  imagesTable: {
    profilePicture:
      "https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/placeholders/placeholder-image@2x.png",
    companyLogo:
      "https://cdn2.hubspot.net/hubfs/53/tools/email-signature-generator/placeholders/logo-placeholder@2x.png",
    customCtaCopy: "",
    customCtaUrl: "",
    ctaColor: "7075DB",
    ctaTextColor: "FFFFFF",
    ctaImage: "",
    customCtaUrlV2: "",
  },
}

/**
 * Context for the application state.
 */
const AppStateContext = createContext()

/**
 * Provider component for managing the application state using useReducer.
 * @param {Object} props - The component's props.
 * @param {JSX.Element} props.children - The child components.
 * @returns {JSX.Element} The AppStateProvider component.
 */
export const AppStateProvider = ({ children }) => {
  const [state, dispatch] = useReducer(appReducer, initialState)

  return (
    <AppStateContext.Provider value={{ state, dispatch }}>
      {children}
    </AppStateContext.Provider>
  )
}

/**
 * Custom hook to access the application state and dispatch.
 * @returns {Object} An object containing the application state and dispatch function.
 */
export const useAppState = () => {
  return useContext(AppStateContext)
}

/**
 * Reducer function for handling state changes.
 * @param {Object} state - The current application state.
 * @param {Object} action - The action to be performed.
 * @returns {Object} The new application state.
 * @throws {Error} Throws an error for unknown actions.
 */
const appReducer = (state, action) => {
  switch (action.type) {
    case "TOGGLE_FAQ_CONTENT": {
      return {
        ...state,
        displayFaq: !state.displayFaq,
      }
    }
    case "CHANGE_NAVBAR_TAB": {
      return {
        ...state,
        oldTableSelected: state.tableSelected,
        tableSelected: action.tableSelected,
      }
    }
    case "RESET_VALUES": {
      return {
        ...state,
        textsTable: initialState.textsTable,
        styleTable: initialState.styleTable,
        imagesTable: initialState.imagesTable,
      }
    }
    case "TOGGLE_FOOTPRINT_CHECKBOX": {
      return {
        ...state,
        templateTable: {
          ...state.templateTable,
          isFootprintSelected: !state.templateTable.isFootprintSelected,
        },
      }
    }
    case "UPDATE_TEMPLATE": {
      return {
        ...state,
        templateTable: {
          ...state.templateTable,
          templateSelected: action.templateSelected,
        },
      }
    }
    case "CHANGE_TEXTTABLE_INPUT": {
      // If all inputs are the same or empty, set the initial state.
      // If only one input has a value different from the initial state and is not empty, update that value and reset the others.
      // If multiple inputs have values, update only those values.
      const updatedTextsTable = {
        ...state.textsTable,
        [action.inputKey]: action.inputValue,
      }

      const isTextsTableEqualToInitialState = Object.keys(
        initialState.textsTable
      ).every(
        (key) =>
          updatedTextsTable[key] === initialState.textsTable[key] ||
          updatedTextsTable[key] === ""
      )

      if (isTextsTableEqualToInitialState) {
        return {
          ...state,
          textsTable: initialState.textsTable,
        }
      }

      const hasSingleNonEmptyDifference = (updatedTextsTable) => {
        let hasDifference = false
        let hasNonEmptyDifference = false

        for (const key in updatedTextsTable) {
          if (updatedTextsTable[key] !== initialState.textsTable[key]) {
            if (updatedTextsTable[key] !== "" && !hasNonEmptyDifference) {
              hasNonEmptyDifference = true
            } else {
              hasDifference = true
              break
            }
          }
        }

        return hasNonEmptyDifference && !hasDifference
      }

      if (hasSingleNonEmptyDifference(updatedTextsTable)) {
        const differentKey = Object.keys(updatedTextsTable).find(
          (key) =>
            updatedTextsTable[key] !== initialState.textsTable[key] &&
            updatedTextsTable[key] !== ""
        )

        const newTextsTable = Object.keys(initialState.textsTable).reduce(
          (result, key) => {
            result[key] = key === differentKey ? updatedTextsTable[key] : ""
            return result
          },
          {}
        )

        return {
          ...state,
          textsTable: newTextsTable,
        }
      } else {
        return {
          ...state,
          textsTable: updatedTextsTable,
        }
      }
    }
    case "CHANGE_STYLE_INPUT": {
      return {
        ...state,
        styleTable: {
          ...state.styleTable,
          [action.inputKey]: action.inputValue,
        },
      }
    }
    case "CHANGE_FONT_SIZE": {
      return {
        ...state,
        styleTable: {
          ...state.styleTable,
          idTextSize: action.idTextSize,
          jobTextSize: action.jobTextSize,
        },
      }
    }
    case "CHANGE_IMAGES_INPUT": {
      return {
        ...state,
        imagesTable: {
          ...state.imagesTable,
          [action.inputKey]: action.inputValue,
        },
      }
    }
  }
  // TODO -> Throw error or not. Currently not to avoid the app to shuting down
  // throw Error("Unknown action: " + action.type)
}
