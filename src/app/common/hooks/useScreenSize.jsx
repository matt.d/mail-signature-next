import { useEffect, useState } from "react"

/**
 * Custom hook for tracking the current screen size.
 * @returns {Object} An object containing the current screen width and height.
 */
export default function useScreenSize() {
  // Check if the code is running on the client side to prevent server-side errors.
  const isClient = typeof window !== "undefined"

  // Initialize the state with current screen width and height, if available.
  const [screenSize, setScreenSize] = useState({
    width: isClient ? window.innerWidth : undefined,
    height: isClient ? window.innerHeight : undefined,
  })

  // Effect to update screen size when the window is resized.
  useEffect(() => {
    // Function to update the screen size state.
    const handleScreenSizeChange = () => {
      if (isClient) {
        setScreenSize({
          width: window.innerWidth,
          height: window.innerHeight,
        })
      }
    }

    if (isClient) {
      window.addEventListener("resize", handleScreenSizeChange) // Add event listener to handle screen size changes.
    }

    return () => {
      if (isClient) {
        window.removeEventListener("resize", handleScreenSizeChange) // Remove the event listener when the component unmounts.
      }
    }
  }, [isClient])

  return screenSize
}
