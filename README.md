# Table of Contents

1. [Context](#context)
   - [Introduction](#introduction)
   - [Project Purpose](#project-purpose)
2. [Install](#install)
   - [Installation](#installation)
   - [Dependencies](#dependencies)
3. [Organization](#organization)
   - [Project Organization on Gitlab](#project-organization-on-gitlab)
4. [Version Control and Documentation](#version-control-and-documentation)
   - [Git Workflow](#git-workflow)
   - [Git Commit Convention](#git-commit-convention)
   - [Code Documentation with JSDoc](#code-documentation-with-jsdoc)
5. [Code Testing](#code-testing)
   - [Unitary Tests](#unitary-tests)
   - [Continuous Integration on Gitlab](#continuous-integration-on-gitlab)
   - [Deploy with Vercel](#deploy-with-vercel)
6. [Miscellaneous](#miscellaneous)
   - [To-Do List](#to-do-list)

# Context

## Introduction

You can give a look of this app by clicking https://mail-signature-next.vercel.app.

This project is a React-based web application developed to demonstrate my skills as a frontend developer. It draws inspiration from HubSpot's email signature generator and showcases my proficiency in HTML, CSS, JavaScript, and React, using the Next.js framework. The project is an ongoing work to further refine my skills as a junior React developer.

**Disclaimer :** This project is based on the [Hubspot email signature generator](hubspot.com/email-signature-generator), which is a proprietary application developed and owned by HubSpot, Inc. (Copyright © 2023 HubSpot, Inc.).

## Project Purpose

The main purpose of this project is to improve my skills as a frontend developer and gain experience in React development. It serves as a portfolio piece to demonstrate my abilities to potential employers. The project is still in progress, with a to-do list outlining pending tasks.

# Install

## Installation

To run this project locally, follow these steps:

- Clone the repository to your local machine.
- Navigate to the project directory.
- Run `npm install` to install project dependencies.
- Start the development server using `npm run dev`.
- Access the application at `http://localhost:3000` in your web browser.

## Dependencies

The main dependencies used in this project include:

- React
- React DOM
- Next.js
- Tailwind CSS
- Framer motion
- React colorful

# Organization

## Project Organization on Gitlab

In actuality, I've structured my project around primary groups that align with sprint cycles. Within these groups, there are sub-groups, each dedicated to more specific objectives and associated with corresponding merge requests to facilitate tracking of changes.

You can easily get a look to the [board issue](https://gitlab.com/matt.d/mail-signature-next/-/boards) or the [list of issues](https://gitlab.com/matt.d/mail-signature-next/-/issues) on as my repository is public.

To efficiently manage the project, I leveraged GitLab's issue board. This board features four labeled lists: 'todo,' 'doing,' 'pending,' and 'done.' This setup simplifies task monitoring, making it clear for both myself and any other developers to know what tasks he/she could works on it and what has been already completed. Here is an overview of that **issue board** :

<div style="text-align: center;">
  <img src="./public/gitlab-issue-board.png" alt="gitlab issue boards" width="1300" height="auto">
</div>

# Version Control and Documentation

## Git Workflow

I use a well-known Git workflow that involves different types of branches:

- **Main**: The primary branch for stable code.
- **Dev**: Where active development takes place.
- **Feature**: Branches for new features.
- **Release**: For preparing and testing releases.
- **Hotfix**: Addressing critical issues quickly.

This approach helps maintain an organized and efficient development process, making it easier to work on new features and fix problems while keeping the main codebase stable. Additionally, it aids in tracking and comprehending the progress made during retrospectives or retroplanning sessions.

<div style="text-align: center;">
  <img src="https://wac-cdn.atlassian.com/dam/jcr:cc0b526e-adb7-4d45-874e-9bcea9898b4a/04%20Hotfix%20branches.svg?cdnVersion=1283" alt="git workflow" width="600" height="600">
</div>

## Git Commit Convention

For the Git commit's name convention, I use the [Angular convention](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines) but add remove useless part for this project in order to reduce the complexity and keep it simple.

### Commit message template :

```
<type>: <subject>
```

### The type could bo different nature :

- **build:** Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- **ci:** Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
- **docs:** Documentation only changes
- **feat:** A new feature
- **fix:** A bug fix
- **perf:** A code change that improves performance
- **refactor:** A code change that neither fixes a bug nor adds a feature
- **style:** Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- **test:** Adding missing tests or correcting existing tests

### The subject contains a succinct description of the change:

- use the imperative, present tense: "change" not "changed" nor "changes"
- don't capitalize the first letter
- no dot (.) at the end

## Code Documentation with JSDoc

In this project, I've made it easy for you to document the code by using JSDoc, a helpful tool for generating clear and consistent documentation. Here's how you can get started:

1. **Adding Comments**: For any function, variable, or class you want to document, simply add comments right before the code.

2. **Using JSDoc Tags**: Inside your comments, you can use JSDoc tags like `@param`, `@returns`, and `@description` to provide details about the purpose, parameters, and return values of your code.

3. **Generating Documentation**: When you're ready, you can use a tool or IDE that supports JSDoc (like VSCode) to automatically generate documentation based on your comments.

In order to generate the static site with all the documentation of this project, just run :

```
npm run jsdoc
```

The result will go in a root folder named **docs/**.

For more information on JSDoc and its usage, you can refer to the [official documentation](https://jsdoc.app/).

# Code Testing

## Unitary Tests

Unit testing is a crucial aspect of ensuring the reliability and functionality of this project. It involves testing individual components and functions to validate that they work as expected. Here's an overview of the testing stack used and the tests that have been implemented:

### Testing Stack

- Testing Framework: Jest
- Testing Library: Testing Library for React
- Mocking: Jest provides mocking capabilities for functions and modules (not used for the moment)
- State Management Testing: `@testing-library/react` and custom test providers for state management.

### Implemented Unit Tests

#### Home Component Test (home.test.js)

The Home component is the main component of the project and serves as the entry point. This test ensures that the Home component renders correctly.

```jsx
import React from "react"
import { render, screen } from "@testing-library/react"

import Home from "@/app/page"

test("Home component renders a heading with the correct text", () => {
  render(<Home />)

  const heading = screen.getByText("Welcome to My Project")
  expect(heading).toBeInTheDocument()
})
```

#### LeftPanel Component Test (leftPanel.test.js)

The LeftPanel component is responsible for managing different tabs, and it's crucial that it displays the correct component when a tab is selected. This test covers the behavior for different tabs.

```jsx
import React from "react"
import { fireEvent, render, screen } from "@testing-library/react"

import { AppStateProvider } from "@/app/common/context/appStateContext"
import LeftPanel from "@/app/modules/leftPanel/leftPanel"

describe("LeftPanel Component", () => {
  it("should display TextsTable when the 'texts' tab is selected", () => {
    // Set up a testing context with the initial state
    const TestComponent = () => {
      return (
        <AppStateProvider>
          <LeftPanel />
        </AppStateProvider>
      )
    }

    const { rerender } = render(<TestComponent />)

    // Simulate a user interaction with the 'texts' tab in the navbar
    fireEvent.click(screen.getByTestId("layout"))

    // Rerender the component to reflect the state change
    rerender(<TestComponent />)

    // Use a test ID or other appropriate selectors to locate the TextsTable component
    const textsTable = screen.getByTestId("template-table")

    expect(textsTable).toBeInTheDocument()
  })

  // ... other cases
})
```

Please note that while these unit tests provide coverage for just a few functionality, there are many more tests on the to-do list. This includes testing edge cases, asynchronous operations, and error handling. **The current tests serve as the foundation** to ensure a smooth setup of the testing environment and establish a testing culture for this project.

## Continuous Integration on Gitlab

### Automated Dependency Updates with Dependabot on GitLab

In my Next.js project on GitLab, I've streamlined dependency management using Dependabot. This tool automates the process of keeping dependencies up to date, making small, regular updates a breeze.

### Why It Matters

Dependency updates often get neglected, but they're crucial for project growth. Small, incremental updates can prevent painful, massive overhauls. Dependabot keeps your project's dependencies current and secure.

### The Tool: Dependabot

Dependabot checks your project's dependencies daily and opens pull requests for any outdated requirements it finds. You review and merge these PRs, ensuring your project stays up to date and secure.

### How I Set It Up

Here's a concise overview of the setup:

1. **Create Access Tokens**: I created GitLab and GitHub personal access tokens to enable Dependabot to read and write data.

2. **Add Environment Variables**: I added these access tokens as environment variables to my project's repository.

3. **Schedule Updates**: I set Dependabot to run weekly, reducing the frequency of updates and making the process more manageable.

4. **Dependabot Configuration**: I created a `dependabot.yml` file to specify how Dependabot should update dependencies.

5. **GitLab Pipeline**: I configured a `.gitlab-ci.yml` file to handle the GitLab pipeline for Dependabot. This file loads the Dependabot Docker image and executes updates based on the schedule.

With these steps, I keep my project's dependencies current with minimal effort. Dependabot periodically creates merge requests with updates, and I review and merge them to ensure the project's growth and security.

For more details and customization options, check the Dependabot documentation and GitLab CI/CD documentation. Happy coding!

## Deploy with Vercel

This app is automatically deployed to https://mail-signature-next.vercel.app each time there is a push to **main** branch.

# Miscellaneous

## To-Do List

Non-exhaustive list of action to do in order to improve this project:

- Display an error message if it's impossible to load the image from the provided URL.
- Avoid using uncontrolled fields for reading images to prevent the app from crashing when loading the URL with every new character entered in the input.
- Replace the three buttons (small, medium, large) with a font-size scrollbar.
- Add an empty footer to each tab.
- Improve the appearance and disappearance of elements in the modals.
- Create the "FAQs" content.
- Ensure the CTA hyperlink is correct, and if the user does not provide a valid URL with "https://," it should not prepend "localhost:3000/href."
- Redirect the hyperlink to "localhost:3000/href" if no "https://..." is provided by the user.
- Implement regex control on input values.
- Display an error message if the "Create signature" button is clicked, and the mandatory fields are not filled out.
- Enhance the HTML retrieval process to avoid double instantiation of modal components (one for UI and the other for HTML retrieval).
- Generate an HTML file directly from the app or find a more convenient solution for copying and pasting the result.
- Improve interactions between different instances of the color picker. The solution is to explicitly set the state to "false" rather than "true." When set to "true," there were unwanted interactions.
- Enhance the formatting when copying the signature.
- Consider using Storybook or the Astro template to create a design system: Astro Design System.
